package section3_apis.part2_collections;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.ArrayList;

public class StudentAdminDataReader {


    private StudentAdmin studentAdmin;
    private static List<Student> students = new ArrayList<>();
    private static List<Course> courses = new ArrayList<>();

    public StudentAdmin importAll(String studentsFile, String courseResultsFile) {

        this.studentAdmin = new StudentAdmin();
        readStudents(studentsFile);
        readCourseResults(courseResultsFile);

        return studentAdmin;
    }

    static List<Student> getStudents(){
        return Collections.unmodifiableList(students);
    }

    public static List<Course> getCourses(){
        return Collections.unmodifiableList(courses);
    }

    private void readStudents(String studentsFile) {
        Path filePath = initFile(studentsFile);
        readFile(filePath, new StudentLineHandler());

    }

    private void readCourseResults(String courseResultsFile) {
        Path filePath = initFile(courseResultsFile);
        readFile(filePath, new CoursesLineHandler());

    }

    private void readFile(Path filePath, LineHandler lineHandler) {
        int lineCount = 0;
        try (BufferedReader reader = Files.newBufferedReader(filePath)) {
            String line;
            while ((line = reader.readLine()) != null) {

                lineCount++;
                if (lineCount == 1) continue;
                //System.out.println("line = " + line);
                lineHandler.processLine(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Path initFile(String fileName) {
        Path path = Paths.get(fileName);
        File file = path.toFile();
        if (! (file.exists() && file.canRead())) {
            throw new IllegalArgumentException("file " + file + " does not exist or is not readable.");
        }
        return path;
    }

    private interface LineHandler {
        void processLine(String line);
    }

    /**
     * processes each line of the file students.txt
     */
    private class StudentLineHandler implements LineHandler {
        @Override
        public void processLine(String line) {
            String[] elements = line.split("\t");
//            System.out.println(Arrays.toString(elements));
//
//            System.out.println(elements[0]);

            int studentId = Integer.parseInt(elements[0]);

            String firstName = elements[1];

            String lastName = elements[2];

            //-------------------  YOUR CODE HERE   -------------------//
            //cells.add(Cell.of(cellType));
            students.add(new Student(studentId,firstName,lastName));

        }
    }

    /**
     * processes each line of the file courses.csv
     */
    private class CoursesLineHandler implements LineHandler {
        @Override
        public void processLine(String line) {
            String[] elements = line.split(";");
//            System.out.println(Arrays.toString(elements));
            //-------------------  YOUR CODE HERE   -------------------//
            String courseId = elements[0];
            String studentId = elements[1];

            double grade = Double.parseDouble(elements[2]);

            Course course = new Course(courseId);
            course.setGrade(grade);
            course.setStudentId(studentId);

            courses.add(course);


        }
    }


    public static void main(String[] args) {
        StudentAdminDataReader dataReader = new StudentAdminDataReader();
        dataReader.importAll("data/students.txt", "data/courses.csv");


        StudentAdmin studentAdmin = new StudentAdmin();
        Student student = new Student(10767, "Larae", "Rowell");
        Course course = new Course("INTRO_JAVA");

        studentAdmin.getGrade(student, course);





    }
}
