package section3_apis.part2_collections;

import java.util.Objects;

public class Course {
    private String courseId;
    private int studentId;
    private double grade;


    public Course(final String courseId) {
        this.courseId = courseId;

    }

    public String getCourseId() {
        return this.courseId;
    }

    public int getStudentId(){
        return this.studentId;
    }

    public void setStudentId(String studentId){
        int student = Integer.parseInt(studentId);

        this.studentId = student;
    }

    public void setGrade(double grade){
        this.grade = grade;
    }

    public double getGrade(){
        return this.grade;
    }




    @Override
    public String toString() {
        return "Course{" +
                "courseId='" + courseId + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final Course course = (Course) o;
        return this.courseId.equals(course.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.courseId);
    }
}
