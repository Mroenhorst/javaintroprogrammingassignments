package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class ProteinAccesionNumberComparator implements Comparator<Protein> {
    @Override
    public int compare(Protein first, Protein second) {
        System.out.println("JEMAMA");

        return first.getAccession().compareToIgnoreCase(second.getAccession());

    }
}
