package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class ProteinNameComparator implements Comparator<Protein> {
    @Override
    public int compare(Protein first, Protein second) {
        return first.getName().compareTo(second.getName());

    }
}
