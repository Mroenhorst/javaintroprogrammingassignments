/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section3_apis.part3_protein_sorting;

import java.util.Comparator;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {
    private final String name;
    private final String accession;
    private final String aminoAcidSequence;
    private GOannotation goAnnotation;

    /**
     * constructs without GO annotation;
     * @param name
     * @param accession
     * @param aminoAcidSequence 
     */
    public Protein(String name, String accession, String aminoAcidSequence) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
    }

    /**
     * construicts with main properties.
     * @param name
     * @param accession
     * @param aminoAcidSequence
     * @param goAnnotation 
     */
    public Protein(String name, String accession, String aminoAcidSequence, GOannotation goAnnotation) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
        this.goAnnotation = goAnnotation;
    }

    /**
     * sorts the proteins by name
     * @param o
     * @return
     */
    @Override
    public int compareTo(Protein o) {

        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;




        return 0;
    }
    
    /**
     * provides a range of possible sorters, based on the type that is requested.
     * @param type
     * @return proteinSorter
     */
    public static Comparator<Protein> getSorter(SortingType type) {

        Comparator<Protein> proteinSorter = null;

        switch (type){
            case PROTEIN_NAME:
                System.out.println("TEST!@#");
                proteinSorter = new ProteinNameComparator();
                break;

            case ACCESSION_NUMBER:
                proteinSorter = new ProteinAccesionNumberComparator();
                break;

            case GO_ANNOTATION:
                proteinSorter = new ProteinGoAnnotationComparator();
                break;

        }


        return proteinSorter;
    }

    /**
     *
     * @return name the name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return accession the accession number
     */
    public String getAccession() {
        return accession;
    }

    /**
     *
     * @return aminoAcidSequence the amino acid sequence
     */
    public String getAminoAcidSequence() {
        return aminoAcidSequence;
    }

    /**
     *
     * @return GO annotation
     */
    public GOannotation getGoAnnotation() {
        return goAnnotation;
    }

    @Override
    public String toString() {
        return "Protein " + goAnnotation.getBiologicalProcess() + "      cellular: " + goAnnotation.getCellularComponent() + "    molecular: " + goAnnotation.getMolecularFunction();
       // return "Protein{" + "name=" + name + ", accession=" + accession + ", aminoAcidSequence=" + aminoAcidSequence + '}';
    }

}
