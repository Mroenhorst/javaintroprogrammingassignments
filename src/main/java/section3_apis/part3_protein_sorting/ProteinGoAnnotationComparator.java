package section3_apis.part3_protein_sorting;

import java.util.Comparator;

public class ProteinGoAnnotationComparator implements Comparator<Protein> {
    @Override
    public int compare(Protein first, Protein second) {

        GOannotation firstAnnotation = first.getGoAnnotation();
        GOannotation secondAnnotation = second.getGoAnnotation();


        int biologicalProcess =  firstAnnotation.getBiologicalProcess().compareTo(secondAnnotation.getBiologicalProcess());

        if (biologicalProcess == 0){

            int cellular =  firstAnnotation.getCellularComponent().compareTo(secondAnnotation.getCellularComponent());

            if (cellular == 0){
                int molecular = firstAnnotation.getMolecularFunction().compareTo(secondAnnotation.getMolecularFunction());

                if (molecular == 0) {
                    return molecular;
                }
            }

            return cellular;

        }

        return biologicalProcess;



    }
}
