package section2_syntax.part1_datatypes;

/**
 * Creation date: May 28, 2018
 *
 * @author Michiel Noback (&copy; 2018)
 * @version 0.01
 */
public class Datatypes {
    /*
    * For the following couple of methods, simply return the name of the datatype that best suits
    * the given use case, as efficiently as possible.
    * Choose from
    *   - boolean
    *   - char
    *   - byte
    *   - short
    *   - int
    *   - long
    *   - double
    *   - float
    *   - String
    *
    * The first one has already been implemented as example.
    */

    String correctDataType0() {
        //USE CASE / USAGE:
        //Number of days within a year
        return "short";
    }

    String correctDataType1() {
        //USE CASE / USAGE:
        //Age of the universe in whole years
        return "long";
    }

    String correctDataType2() {
        //USE CASE / USAGE:
        //Turnover of ATP in a cell, in Molar per minute
        return "float";
    }

    String correctDataType3() {
        //USE CASE / USAGE:
        //Molecular weight of a protein, in Daltons
        return "float";
    }

    String correctDataType4() {
        //USE CASE / USAGE
        // the live/death status of a test animal
        return "boolean";
    }

    String correctDataType5() {
        //USE CASE / USAGE
        // the name of an app user
        return "String";
    }
    String correctDataType6() {
        //USE CASE / USAGE
        // encoding of human gender (Male, Female, Undefined)
        return "char";
    }

    /**
     * The method below is supposed to calculate the G+C fraction of a DNA sequence.
     * It should do so case insensitive.
     * After uncommenting the method body (select the blok and press Ctrl + /), there are 3 problems and one error
     * in this method you should find and fix before the tests will pass.
     */
    double determineGCfraction(String theDNA) {
        int gcCount = 0;
        for (int i = 0; i < theDNA.length(); i++) {
            char nucleotide = theDNA.charAt(i);


            if (Character.toUpperCase(nucleotide) == 'C' || Character.toUpperCase(nucleotide) == 'G') {
                gcCount++;
            }
        }
        double fraction = (double) gcCount/ (double) theDNA.length();
        return fraction;
    }

    /**
     * The method below creates a string and calls on another method to change it.
     * Something goes wrong - can you fix it?
     */
    String modifyString() {
        String input = "Waar kan ik de koffiemachine vinden in dit gebouw?";
        input = replaceWord(input, "koffie", "soda");
        return input;
    }

    String replaceWord(String input, String replace, String replaceWith) {
        return input.replace(replace, replaceWith);
    }

    /**
     * The method below should return a String array of length 2, with the first and last element
     * of the input array in it, converted to String representations.
     * Hint: look at the methods available to class Object (GIYF)
     */
    String[] getFirstAndLast(Object[] input) {

        // retrieve first object from array input
        Object first = input[0];

        // determine the length of array, minus 1 to index the last item in the array
        int lastItem = input.length - 1;

        // retrieve the last object from array input
        Object last = input[lastItem];

        // make array and insert objects first and last and convert to String representation
        String[] getFirstAndLastItem = {first.toString(), last.toString()};


        // return array with the first and the last item from array input
        return getFirstAndLastItem;
    }


    /**
     * This method should return a new int array where all integers from the input have been cubed (power of 3).
     * For convenience, the skeleton of a for loop is already provided.
     * @param input
     * @return cubedInput
     */
    int[] cubeAll(int[] input) {
        int arrayLength = input.length; //get the correct value
        int[] cubedInts = new int[arrayLength];

        for(int i = 0; i < arrayLength; i++) {
            double doubledInt = (double) input[i];
            double cubedDouble = Math.pow(doubledInt, 3);
            cubedInts[i] = (int) cubedDouble;

        }
        return cubedInts;
    }

    /**
     * This method should return the cumulative product of all numbers in the input array.
     * @param input
     * @return cumProd the cumulative product
     */
    int cumulativeProduct(int[] input) {
        return 0;
    }

}
